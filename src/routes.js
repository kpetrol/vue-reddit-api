import Vue from 'vue';
import Router from 'vue-router';
import PostsList from './components/PostsList/PostsList';
import PostDetil from './components/PostDetail/PostDetail'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'post-list',
            component: PostsList
        },
        {
            path: '/:category/:id',
            name: 'post-detail',
            component: PostDetil
        } 
    ]
})